# Proquu

## Overview
Proquu is a simple requirements and test management application created with [StrongLoop](https://strongloop.com/), [LoopBack](https://strongloop.com/node-js/loopback-framework/) and [Bluemix Mobile Services](http://www.ibm.com/cloud-computing/bluemix/solutions/mobilefirst/). 